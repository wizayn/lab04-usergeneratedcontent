﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(TriviaReferences))]
public class TriviaLoadScript : MonoBehaviour 
{

	FileInfo originalFile;
	TextAsset textFile;
	TextReader reader;
	TriviaReferences refs;


	public List<string> questions = new List<string>();
	public List<string> answers = new List<string>();

	// Use this for initialization
	void Start()

	{
		refs = gameObject.GetComponent<TriviaReferences>();

		originalFile = new FileInfo(Application.dataPath + "/triviaQuestions.txt");

		if (originalFile != null && originalFile.Exists)
		{
			reader = originalFile.OpenText();
		}
		else
		{
			textFile = (TextAsset)Resources.Load("triviaEmbedded", typeof(TextAsset));
			reader = new StringReader(textFile.text);
		}

		string lineOfText;
		int lineNumber = 0;

		//tell the reader to read a line of text, and store that in the lineOfTextVariable
		//continue doing this until there are no lines left
		while ((lineOfText = reader.ReadLine()) != null)
		{
			if (lineNumber % 6 == 0)
			{
				questions.Add(lineOfText);
			}
			else if(lineNumber - 1 % 5 == 0)
			{
				refs.correctAnswer = lineOfText;
				answers.Add(lineOfText);
			}
			else
			{
				answers.Add(lineOfText);
			}

			lineNumber++;
		}
		SendMessage("Gather");
	}
}
