﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TriviaReferences : MonoBehaviour {

	[HideInInspector]
	public string correctAnswer;
	[HideInInspector]	
	public int randomQuestion;
	[HideInInspector]
	public string answer1;
	[HideInInspector]
	public string answer2;
	[HideInInspector]
	public string answer3;
	[HideInInspector]
	public string answer4;

	public List<string> questions = new List<string>();
	[HideInInspector]
	public List<string> answers = new List<string>();

	void Gather ()
	{
		questions = GetComponent<TriviaLoadScript> ().questions;
		answers = GetComponent<TriviaLoadScript> ().answers;

		if (randomQuestion == 0) 
		{

		}
		SendMessage("Begin");
	}

}
