﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(TriviaReferences))]
public class TriviaEngine : MonoBehaviour {

	TriviaReferences refs;
	public int answeredCorrectly = 0;
	public int answeredWrong = 0;
	string userChoice = "";
	string question = "";
	string correctAnswer = "";
	public Text questionText;
	public Text answer1;
	public Text answer2;
	public Text answer3;
	public Text answer4;
	int buttonClicked;
	public int randomQuestion;

	void Begin()
	{
		//Gets a link to the references script
		refs = gameObject.GetComponent<TriviaReferences>();
		//Starts the game
		StartGame();
	}

	public void StartGame()
	{
		userChoice = "";
		question = "";
		correctAnswer = "";

		int numQuestions = refs.questions.Count;
		randomQuestion = Random.Range (0, numQuestions);

		question = refs.questions[randomQuestion];
		questionText.text = refs.questions[randomQuestion];

		if (randomQuestion == 0) 
		{
			correctAnswer = refs.answers [4];
			answer1.text = refs.answers [0];
			answer2.text = refs.answers [1];
			answer3.text = refs.answers [2];
			answer4.text = refs.answers [3];

		} 
		else if (randomQuestion == 1) 
		{
			correctAnswer = refs.answers [9];
			answer1.text = refs.answers [5];
			answer2.text = refs.answers [6];
			answer3.text = refs.answers [7];
			answer4.text = refs.answers [8];
		} 
		else if (randomQuestion == 2) 
		{
			correctAnswer = refs.answers [14];
			answer1.text = refs.answers [10];
			answer2.text = refs.answers [11];
			answer3.text = refs.answers [12];
			answer4.text = refs.answers [13];
		}
	}

	public void AnswerFieldOneClicked()
	{
		userChoice = answer1.text;
		Debug.Log ("correctAnswer " + correctAnswer + " Answer " + answer1.text + " User Choice " + userChoice);
		if (userChoice == correctAnswer)
		{
			answeredCorrectly++;
			StartGame ();
		} 
		else 
		{
			answeredWrong++;
			StartGame();
		}
	}
	public void AnswerFieldTwoClicked()
	{
		userChoice = answer2.text;
		Debug.Log ("correctAnswer " + correctAnswer + " Answer " + answer1.text + " User Choice " + userChoice);
		if (userChoice == correctAnswer) {
			answeredCorrectly++;
			StartGame ();
		}
		else 
		{
			answeredWrong++;
			StartGame();
		}
	}
	public void AnswerFieldThreeClicked()
	{
		userChoice = answer3.text;
		Debug.Log ("correctAnswer " + correctAnswer + " Answer " + answer1.text + " User Choice " + userChoice);
		if (userChoice == correctAnswer) {
			StartGame ();
		}
		else 
		{
			answeredWrong++;
			StartGame();
		}
	}
	public void AnswerFieldFourClicked()
	{
		userChoice = answer4.text;
		Debug.Log ("correctAnswer " + correctAnswer + " Answer " + answer1.text + " User Choice " + userChoice);
		if (userChoice == correctAnswer) {
			StartGame ();
		}
		else 
		{
			answeredWrong++;
			StartGame();
		}
	}
}
